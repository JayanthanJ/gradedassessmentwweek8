package org.websparrow.dao;

import java.util.List;

import org.websparrow.model.Books;
import org.websparrow.model.User;

public interface UserDao {

	public int registerUser(User user);

	public String loginUser(User user);
	
	public List<Books> getBooks();
	
	public boolean inserter(int id);
	public List<Books> getFavBooks();
	public boolean inserterlater(int id);
	public List<Books> getreadBooks();
}
