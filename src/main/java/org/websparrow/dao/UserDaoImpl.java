package org.websparrow.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.websparrow.model.Books;
import org.websparrow.model.User;

public class UserDaoImpl implements UserDao {

	private JdbcTemplate jdbcTemplate;

	public UserDaoImpl(DataSource dataSoruce) {
		jdbcTemplate = new JdbcTemplate(dataSoruce);
	}

	@Override
	public int registerUser(User user) {
		
		String sql = "INSERT INTO USER_DATA VALUES(?,?)";

		try {
			
			int counter = jdbcTemplate.update(sql, new Object[] { user.getUserId(), user.getPassword() });

			return counter;

		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public String loginUser(User user) {
		
		String sql = "SELECT USER_ID FROM USER_DATA WHERE USER_ID=? AND USER_PASS=?";
		
		try {

			String userId = jdbcTemplate.queryForObject(sql, new Object[] {
					user.getUserId(), user.getPassword() }, String.class);

			return userId;
			
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<Books> getBooks()
	{
		String sql="select * from Bookss";
		List<Books> listBooks = jdbcTemplate.query(sql, new RowMapper<Books>() {

			@Override
			public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
				Books book=new Books();
				book.setId(rs.getInt(1));
				book.setTitle(rs.getString(2));
				book.setAuthorName(rs.getString(3));
				book.setGenre(rs.getString(4));
				book.setPrice(rs.getDouble(5));
				
				return book;
			}
		});
		return listBooks;
	}
	@Override
	public boolean inserter(int id) {
		String sqls = "INSERT INTO favs(id) VALUES(?)";
		try {
			
			
			jdbcTemplate.update(sqls, new Object[] { id});


		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}
	 

	
	@Override
	public List<Books> getFavBooks()
	{
		String sql="select f.rid,f.id,b.Title,b.authorname,b.genre,b.Price from favs f left join bookss b on f.id=b.id;";
		List<Books> listBooks = jdbcTemplate.query(sql, new RowMapper<Books>() {

			@Override
			public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
				Books book=new Books();
				book.setId(rs.getInt(2));
				book.setTitle(rs.getString(3));
				book.setAuthorName(rs.getString(4));
				book.setGenre(rs.getString(5));
				book.setPrice(rs.getDouble(6));
				
				return book;
			}
		});
		return listBooks;
	}
	
	@Override
	public boolean inserterlater(int id) {
		String sqls = "INSERT INTO readss(id) VALUES(?)";
		try {
			
			
			jdbcTemplate.update(sqls, new Object[] { id});


		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return true;
	}
	 

	
	@Override
	public List<Books> getreadBooks()
	{
		String sql="select f.rid,f.id,b.Title,b.authorname,b.genre,b.Price from readss f left join bookss b on f.id=b.id;";
		List<Books> listBooks = jdbcTemplate.query(sql, new RowMapper<Books>() {

			@Override
			public Books mapRow(ResultSet rs, int rowNum) throws SQLException {
				Books book=new Books();
				book.setId(rs.getInt(2));
				book.setTitle(rs.getString(3));
				book.setAuthorName(rs.getString(4));
				book.setGenre(rs.getString(5));
				book.setPrice(rs.getDouble(6));
				
				return book;
			}
		});
		return listBooks;
	}
	
	
}