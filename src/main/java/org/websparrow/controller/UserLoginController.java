package org.websparrow.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.websparrow.dao.UserDao;
import org.websparrow.model.Books;
import org.websparrow.model.User;

@Controller
public class UserLoginController {

	@Autowired
	private UserDao userDao;
	
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String userLogin(@RequestParam("userId") String userId, @RequestParam("password") String password,Map<String , String> map,Map<String, List<Books>> map1) {

		ModelAndView mv = new ModelAndView();

		User user = new User();
		user.setUserId(userId);
		user.setPassword(password);

		String name = userDao.loginUser(user);

		if (name != null) {

			List<Books> books=this.userDao.getBooks();
			map1.put("books", books);
			map.put("email", user.getUserId());
			return "welcome";

		} else {

			//mv.addObject("msg", "Invalid user id or password.");
			return "login";
		}
		

	}
	@GetMapping("/book")
	public String getBook(Map<String , String> map,Map<String, List<Books>> map1,HttpSession session) {
		List<Books> books=this.userDao.getBooks();
		map1.put("books", books);
		return "book";
	}
	@PostMapping("/book")
	public String postBook(Map<String , String> map,Map<String, List<Books>> map1,HttpSession session) {
		List<Books> books=this.userDao.getBooks();
		map1.put("books", books);
		return "book";
	}

	@RequestMapping("/favs")
	public String getFavorite1(@RequestParam int id,Map<String, List<Books>> map1) {
		userDao.inserter(id);
		//map1.put("books", books);
		return "redirect:favorite";
	}
	@RequestMapping("/favorite")
	public String getFavorite(Map<String, List<Books>> map1) {
		List<Books> books=this.userDao.getFavBooks();
		map1.put("books", books);
		return "favorite";
		
		
		
	}
	@RequestMapping("/readss")
	public String getread1(@RequestParam int id,Map<String, List<Books>> map1) {
		userDao.inserterlater(id);
		//map1.put("books", books);
		return "redirect:readlater";
	}
	@RequestMapping("/readlater")
	public String getread(Map<String, List<Books>> map1) {
		List<Books> books=this.userDao.getreadBooks();
		map1.put("books", books);
		return "readlater";
		
		
	 
}
}
