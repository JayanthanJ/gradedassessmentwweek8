<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style>
    body
    {
     background-image: url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwje5W7p1A1fhxNXEdYijie8DQwQJymIQLa4FjD-ThtUWDyoNqgGjKfPJvJw2LE5sUm7A&usqp=CAU');
     background-size: cover;
     
    }
   
    </style>
</head>
<body>
<table border="5" cellspacing="15">
		<tr>
			<th><p>ID</p></th>
			<th><p>Title</p></th>
			<th><p>AuthorName</p></th>
			<th><p>Genre</p></th>
			<th><p>Price</p></th>
		</tr>
		
	<c:forEach var="book" items="${books}">
	<tr>
			<th><p>
					${book.id}</p></th>
			<th><p>
					${book.title}</p></th>
			<th><p>
					${book.authorName}</p></th>
			<th><p>
					${book.genre}</p></th>
			<th><p>
					${book.price}</p></th>
			
		</tr>
	
	</c:forEach>
	</table>
</body>
</html>