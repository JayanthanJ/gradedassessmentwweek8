    <%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"
	%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    
<!DOCTYPE html>
<html lang="en">
 <head>

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Document</title>
    <style>
    body
    {
     background-image: url('https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Gutenberg_Bible%2C_Lenox_Copy%2C_New_York_Public_Library%2C_2009._Pic_01.jpg/447px-Gutenberg_Bible%2C_Lenox_Copy%2C_New_York_Public_Library%2C_2009._Pic_01.jpg');
     background-size: cover;
     
    }
   
    </style>
  
   
</head>
<body>
<h1 align="center">
 
 <p>${email}</p>
 
</h1>
 

  <div align="center"><a href="favorite"><button type="button" class="btn btn-info">Favourites</button></a></div><br>
  <div align="center"><a href="readlater"><button type="button" class="btn btn-primary">Read later</button></a></div><br>
  <div align="center"><a href="book"><button type="button" class="btn btn-dark">AllBooks</button></a></div><br>
  <div align="center"><a href="login.jsp"><button type="button" class="btn btn-outline-success">Logout</button></a></div><br>
 
 
</body>
</html>
