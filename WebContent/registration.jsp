<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
 
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
  	.error{
  		color:red;
  		font-size: 20px;
  	}
  </style>
   <style>
    body
    {
     background-image: url('https://media.istockphoto.com/photos/row-of-old-books-with-colorful-covers-on-pastel-blue-background-up-picture-id1126130554?k=20&m=1126130554&s=612x612&w=0&h=eohHCqhH7lUd8l4nxjVf9jGhxrvQbm7Fu8EFZZWbD0o=');
     background-size: cover;
     
    }
   
    </style>
</head>
<body>
	<h2 align="center">BOOK LIBRARIES TO EXPLORE </h2>
<div class="container">
		  <form action="register" method="post">
		  <div class="row">
		  <div class="col-lg-6 col-lg-offset-3">
		 <!--  <div class="form-group">
		      <label for="name">Id:</label>
		      <input type="text" class="form-control" id="cid" placeholder="Enter ID" name="cid">
		    </div> -->
		    <div class="form-group">
		      <label for="name">Name:</label>
		      <input type="text" class="form-control" id="name"
		       placeholder="Enter Name" name="userId">
		    </div>
		    
		    <div class="form-group">
		      <label for="phone">Phone:</label>
		      <input type="number" class="form-control" 
		      id="phone" placeholder="Enter phone no." name="phone">
		    </div>		    
		    <div class="form-group">
		      <label for="email">Email:</label>
		      <input type="email" class="form-control" 
		      id="email" placeholder="Enter email" name="email">
		    </div>	    
		      
		     <div class="form-group">
		      <label for="password">Password:</label>
		      <input type="password" class="form-control" 
		      id="password" placeholder="Enter password" name="password">
		    </div>
		    <div class="form-group">
		      <label for="country">Country:</label>
		      <input type="text" class="form-control" 
		      id="country" placeholder="Enter Country" name="country">
		    </div>
		    
		    <div align="center"><input type="submit" class="btn btn-danger" value="Register"/></div>
		    </div>
		    </div>
		  </form>
</div>
 
</body>
</html>